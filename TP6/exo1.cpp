#include "tp6.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;

void Graph::buildFromAdjenciesMatrix(int **adjacencies, int nodeCount) {
	/**
	  * Make a graph from a matrix
	  * first create all nodes, add it to the graph then connect them
	  * this->appendNewNode
	  * this->nodes[i]->appendNewEdge
	  */

    for(int i = 0; i<nodeCount; i++){
        this->appendNewNode(new GraphNode(i));
    }

    for(int i = 0; i<nodeCount; i++){
        for(int j = 0; j<nodeCount; j++){
            if(adjacencies[i][j]>=1){
                this->nodes[i]->appendNewEdge(this->nodes[j], adjacencies[i][j]);
            }
        }
    }
}

void Graph::deepTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[]) {
	/**
	  * Fill nodes array by travelling graph starting from first and using recursivity
	  */
    visited[first->value] = true;
    Edge* edge = first->edges;

    nodes[nodesSize] = first;
    nodesSize++;

    while(edge != nullptr) {
        GraphNode* destination = edge->destination;

        if (!visited[destination->value]) {
            this->deepTravel(destination, nodes, nodesSize, visited);
        }
        edge = edge->next;
    }
}

void Graph::wideTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[]) {
	/**
	 * Fill nodes array by travelling graph starting from first and using queue
	 * nodeQueue.push(a_node)
	 * nodeQueue.front() -> first node of the queue
	 * nodeQueue.pop() -> remove first node of the queue
	 * nodeQueue.size() -> size of the queue
	 */
	std::queue<GraphNode*> nodeQueue;
	nodeQueue.push(first); 

    while(!nodeQueue.empty()){
        GraphNode* firstNode = nodeQueue.front();
        nodeQueue.pop();

        nodes[nodesSize]=first;
        nodesSize++;

        visited[firstNode->value] = true;

        Edge* edge = first->edges;

        while(edge != nullptr) {
            GraphNode* destination = edge->destination;

            if (!visited[destination->value]) {
                nodes[nodesSize] = destination;
                nodesSize++;

                nodeQueue.push(edge->destination);
                visited[destination->value] = true;
            }
            edge = edge->next;
        }
    }
}

bool Graph::detectCycle(GraphNode *first, bool visited[]) {
	/**
	  Detect if there is cycle when starting from first
	  (the first may not be in the cycle)
	  Think about what's happen when you get an already visited node
    **/

    std::queue<GraphNode*> nodeQueue;
    nodeQueue.push(first);

    while(!nodeQueue.empty()){
        GraphNode* firstNode = nodeQueue.front();
        nodeQueue.pop();

        visited[firstNode->value] = true;

        Edge* edge = first->edges;

        while(edge != nullptr) {
            if(visited[edge->destination->value == false]) {
                nodeQueue.push(edge->destination);
                edge = edge->next;
            }
            else if(edge->destination == first) return true;
        }
    }
    return false;
}

int main(int argc, char *argv[]) {
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 150;
	w = new GraphWindow();
	w->show();

	return a.exec();
}
