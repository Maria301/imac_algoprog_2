#include <iostream>

using namespace std;

struct Noeud {
    int donnee;
    Noeud* suivant;
};

struct Liste {
    Noeud* premier;
    int ajoute();
    int recupere();
    int cherche();
    int stocke();
};

struct DynaTableau{
    int* donnees;
    int capacite;
    int count;
    int ajoute();
    int recupere();
    int cherche();
    int stocke();
};


void initialise(Liste* liste) {
    liste->premier = nullptr;
}

bool est_vide(const Liste* liste) {
    if (liste->premier == nullptr) {
        return true;
    }
    return false;
}

void ajoute(Liste* liste, int valeur) {
    if (est_vide(liste) == true) {
        liste->premier = new Noeud;
        liste->premier->donnee = valeur;
        liste->premier->suivant = nullptr;
    }

    Noeud *noeud = liste->premier;
    while (noeud->suivant != nullptr) {
        noeud = noeud->suivant;
    }
    noeud->suivant = new Noeud;
    noeud->suivant->donnee = valeur;
    noeud->suivant->suivant = nullptr;
}

void affiche(const Liste* liste) {
    if (est_vide(liste)) {
        printf("La liste est vide\n");
    } 
    else {
        Noeud* noeud = liste->premier;
        while (noeud != nullptr) {
            printf("%d\n", noeud->donnee);
            noeud = noeud->suivant;
        }
    }
}

int recupere(const Liste* liste, int n) {
    if (n < 0) {
        printf("Erreur : n ne peut pas être négatif\n");
        return 0;
    }

    Noeud* noeud = liste->premier;
    int i = 0;

    while (noeud != nullptr && i < n) {
        noeud = noeud->suivant;
        i++;
    }

    if (noeud == nullptr) {
        printf("Erreur : n ne peut pas être plus grand que la taille de la liste\n");
        return 0;
    }

    return noeud->donnee;
}

int cherche(const Liste* liste, int valeur) {
    Noeud* noeud = liste->premier;
    int i = 0;

    while (noeud != nullptr) {
        if (noeud->donnee == valeur) {
            return i;
        }
        noeud = noeud->suivant;
        i++;
    }
    return -1;
}

void stocke(Liste* liste, int n, int valeur) {
    if (n < 0) {
        printf("Erreur : n ne peut pas être négatif\n");
        return;
    }

    Noeud* noeud = liste->premier;
    int i = 0;

    while (noeud != nullptr && i < n) {
        noeud = noeud->suivant;
        i++;
    }

    if (noeud == nullptr) {
        printf("Erreur : n ne peut pas être plus grand que la taille de la liste\n");
        return;
    }

    noeud->donnee = valeur;
}

void initialiseT(DynaTableau* tableau, int capacite) {
    tableau->capacite = capacite;
    tableau->count = 0;
    tableau->donnees = (int*) malloc(capacite*sizeof(int));
}

bool est_videT(const DynaTableau* tableau) {
    return tableau->donnees == NULL;
}

void ajoute(DynaTableau* tableau, int valeur) {
    if (est_videT(tableau)) {
        initialiseT(tableau, tableau->capacite);
    }
    else if (tableau->count == tableau->capacite) {
        tableau->capacite *= 2;
        tableau->donnees = (int*) realloc(tableau->donnees, tableau->capacite*sizeof(int));
    }
    tableau->donnees[tableau->count] = valeur;
    tableau->count++;
}

void affiche(const DynaTableau* tableau) {
    if (est_videT(tableau)) {
        printf("Le tableau est vide\n");
    } 
    else {
        for (int i = 0; i < tableau->count; i++) {
            printf("%d\n", tableau->donnees[i]);
        }
    }
}

int recupere(const DynaTableau* tableau, int n) {
    if (n < 0) {
        printf("Erreur : n ne peut pas être négatif\n");
        return 0;
    }
    return tableau->donnees[n];
}

int cherche(const DynaTableau* tableau, int valeur) {
    for (int i = 0; i < tableau->count; i++) {
        if (tableau->donnees[i] == valeur) {
            return i;
        }
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur) {
    if (est_videT(tableau)) {
        printf("Le tableau est vide\n");
    }
    else {
        int i = 0;
        while (i < n-1) {
            i++;
        }
        tableau->donnees[i] = valeur;
    }
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur) {

}

//int retire_file(Liste* liste)
int retire_file(Liste* liste) {
    return 0;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur) {

}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste) {
    return 0;
}

int main() {
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialiseT(&tableau, 5);

    if (!est_vide(&liste)) {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_videT(&tableau)) {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste)) {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_videT(&tableau)) {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    std::cout << std::endl;
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl << std::endl;

    std::cout << "21 se trouve dans la liste a " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans le tableau a " << cherche(&tableau, 15) << std::endl << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements apres stockage de 7 :" << std::endl;
    affiche(&liste);
    std::cout << std::endl;
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0) {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0) {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0) {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0) {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
