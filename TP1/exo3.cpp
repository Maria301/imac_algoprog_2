#include <iostream>
#include <string>
using namespace std;

int search(int value, int tab[], int size) {
	if (size >= 1) {
		if (tab[size] == value) {
			return size;
		}
		else {
			search(value, tab, size-1);
		}       
	}
	else {
		return -1;
	}  
}

int main(int argc, char *argv[]) {
	int size, currentNumber, value = 0;
	int tab[15];

	cout << "Veuillez saisir la taille du tableau : \n";
	cin >> size;

	cout << "Veuillez saisir " << size << " nombres : \n";

	for (int i = 0; i < size; i++) {
	    cin >> currentNumber;
	    tab[i] = currentNumber;
	}

	cout << "Veuillez saisir un nombre pour connaitre son indice dans le tableau : \n";
	cin >> value;

	cout << "L'index correspondant a cette valeur est " << search(value, tab, size);
}




