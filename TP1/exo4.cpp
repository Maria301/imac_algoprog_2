#include <iostream>
#include <string>
using namespace std;

int allEvens(int evens[], int array[], int evenSize, int arraySize) {
	int arrayLength = 10;

  if (arraySize < arrayLength) {		
		if (array[arraySize] % 2 == 0) {
			evens[evenSize] = array[arraySize];
			cout << evens[evenSize] << "\n";
			evenSize++;
		}
		allEvens(evens, array, evenSize, arraySize+1);
	}
}

int main(int argc, char *argv[]) {
	int evens[10];
	int array[10] = {1,2,3,4,5,6,7,8,9,10};
	allEvens(evens, array, 0, 0);
}