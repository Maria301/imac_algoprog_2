#include <iostream>
#include <string>
using namespace std;

int fibonacci(int value) {
    if (value == 0) {return 0;}
    else if (value == 1) {return 1;}

    return fibonacci(value-1) + fibonacci(value-2);
}

int main(int argc, char *argv[]) {
    int number = 0;
    cout << "Veuillez saisir un nombre : ";
    cin >> number;
    cout << "Le nombre de fibonacci correspondant a " << number << " est " << fibonacci(number);
}
