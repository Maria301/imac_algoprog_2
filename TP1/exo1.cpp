#include <iostream>
#include <string>
using namespace std;

long powerNum(long value, long n)
{
    if (n > 1) {value *= powerNum(value, n-1);}
    return value;
}

int main(int argc, char *argv[])
{
    long number, power = 0;
    cout << "Veuillez saisir un nombre : ";
    cin >> number;
    cout << "Veuillez saisir sa puissance : ";
    cin >> power;
    cout << number << " a la puissance de " << power << " est " << powerNum(number, power);
}
