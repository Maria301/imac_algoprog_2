#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChildIndex(int nodeIndex) {
    return 2*nodeIndex + 1;
}

int Heap::rightChildIndex(int nodeIndex) {
    return 2*nodeIndex + 2;
}

void Heap::insertHeapNode(int heapSize, int value) {
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;

    while (i > 0 && this->get(i) > this->get((i-1)/2)) {
        this->swap(this->get(i), this->get((i-1)/2));
        i = (i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex) {
	// use (*this)[i] or this->get(i) to get a value at index i
	int i_max = nodeIndex;
    int leftChild = this->leftChildIndex(nodeIndex);
    int rightChild = this->rightChildIndex(nodeIndex);

    if (leftChild < heapSize && this->get(leftChild) > this->get(i_max)) i_max = leftChild;
    if (rightChild < heapSize && this->get(rightChild) > this->get(i_max)) i_max = rightChild;

    this->swap(nodeIndex, i_max);
    heapify(heapSize, i_max);
}

void Heap::buildHeap(Array& numbers) {
    for (int i = 0; i < numbers.size(); i++) {
        insertHeapNode(i, numbers[i]);
    }
}

void Heap::heapSort() {
    int heap_size = this->size()-1;
    for (int i = heap_size; i > 0; i--)	{
        this->swap(0, i);
        this->heapify(i, 0);
    }
}

int main(int argc, char *argv[]) {
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
