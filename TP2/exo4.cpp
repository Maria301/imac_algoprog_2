#include <QApplication>
#include <time.h>
#include <stdlib.h>

#include "tp2.h"

MainWindow* w=nullptr;

void recursivQuickSort(Array& toSort, int size) {
    Array& lowerArray = w->newArray(size);
    Array& greaterArray= w->newArray(size);

    srand(time(NULL));
    int pivotIndex = rand() % size;
    int pivot = toSort[pivotIndex];
    int lowerSize = 0, greaterSize = 0;

    for (int i=0; i<size; i++) {
        if (toSort[i] < pivot) {
            lowerArray[lowerSize] = toSort[i];
            lowerSize++;
        }
        else if (toSort[i] > pivot) {
            greaterArray[greaterSize] = toSort[i];
            greaterSize++;
        }
    }

    //* Vérification s'il est nécessaire de continuer le tri
    if (lowerSize>1 || greaterSize>1) {
        recursivQuickSort(lowerArray, lowerSize);
        recursivQuickSort(greaterArray, greaterSize);
    }

    //* Remplacement dans le tableau initial
    for (int i = 0; i < lowerSize; i++) {
        toSort[i] = lowerArray[i];
    }

    toSort[lowerSize] = pivot;

    for (int i = 0; i < greaterSize; i++) {
        toSort[lowerSize+1+i] = greaterArray[i];
    }
}

void quickSort(Array& toSort) {
   recursivQuickSort(toSort, toSort.size());
}

int main(int argc, char *argv[]) {
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(quickSort);
	w->show();

	return a.exec();
}
