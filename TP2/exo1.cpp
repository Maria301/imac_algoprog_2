#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort) {
    for (int i=0; i<(int)toSort.size(); i++) {
        int min = toSort[i];
        int indexMin = i;

        for (int j = i+1; j < (int)toSort.size(); j++) {
            if (min > toSort[j]) {
                min = toSort[j];
                indexMin = j;
            }
        }
        
        toSort.swap(indexMin,i);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount = 6; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
